#include "fdf.hpp"

using namespace FDF;

void remove_comment(string& str)
{
	size_t pos;
	if((pos = str.find('#')) == string::npos) return;

	str = string(str.cbegin(), str.cbegin() + pos);
}

void trim(string& str)
{
	while(!str.empty()) {
		if(str.front() != ' ' && str.front() != '\t') break;
		str.erase(0, 1);
	}

	while(!str.empty()) {
		if(str.back() != ' ' && str.back() != '\t') break;
		str.pop_back();
	}
}

inline bool str_contains(const string& str, const char c)
{
	return (str.find(c) != string::npos);
}

void File::add_field(string& str)
{
	remove_comment(str);
	trim(str);
	if(str.empty()) return;

	size_t pos;

	if((pos = str.find('=')) == string::npos) {
		throw invalid_argument("Missing `=` on field declaration!");
	}

	string name(str.cbegin(), str.cbegin() + pos);
	trim(name);
	if(name.empty()) throw invalid_argument("Empty field name!");

	if(str_contains(name, ' ') || str_contains(name, '\t')) {
		throw invalid_argument("Field name contains space or tab!");
	}

	string value(str.cbegin() + pos + 1, str.cend());
	trim(value);
	if(value.empty()) throw invalid_argument("Empty field value!");

	Value val;
	put_value(val, value);

	fields.emplace(name, val);
}

void File::put_value(Value& value, const string& str)
{
	if(str.front() == '"') {
		if(str.size() == 1 || str.back() != '"') {
			throw invalid_argument("Invalid string declaration!");
		}

		value = string(str.cbegin() + 1, str.cend() - 1);
		return;
	}

	if(str == "true" || str == "false") {
		value = (bool) (str == "true");
		return;
	}

	try {
		value = (str_contains(str, '.')
			? (float) atof(str.c_str()) : (int) atoi(str.c_str()));
	} catch(const invalid_argument& e) {
		throw invalid_argument("Invalid value!");
	}
}

void File::operator<<(const string str)
{
	fields.clear();

	stringstream ss(str);
	string buffer;

	size_t i = 0;

	while(getline(ss, buffer)) {
		i++;

		try {
			add_field(buffer);
		} catch(const invalid_argument& e) {
			throw invalid_argument(string(e.what())
				+ " (Line " + to_string(i) + ")");
		}
	}
}

void File::operator<<(ifstream file)
{
	if(!file.is_open()) throw runtime_error("Cannot open file!");

	file.seekg(0, ios::end);
	const auto size = file.tellg();
	file.seekg(0);

	string buffer(size, '\0');
	file.read(&buffer[0], size);

	operator<<(buffer);
}

void File::write(ofstream file) const
{
	for(const auto& f : fields) {
		file << f.first << " = " << f.second.dump() << '\n';
	}

	file.flush();
}
