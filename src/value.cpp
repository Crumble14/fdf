#include "fdf.hpp"

using namespace FDF;

string Value::dump() const
{
	switch(type) {
		case INT_T: {
			return to_string(operator int());
		}

		case FLOAT_T: {
			return to_string(operator float());
		}

		case BOOL_T: {
			return (operator bool() ? "true" : "false");
		}

		case STRING_T: {
			return string("\"") + operator string() + '"';
		}
	}

	return "?";
}

void Value::free()
{
	switch(type) {
		case INT_T: {
			delete (int*) value;
			break;
		}

		case FLOAT_T: {
			delete (float*) value;
			break;
		}

		case BOOL_T: {
			delete (bool*) value;
			break;
		}

		case STRING_T: {
			delete (string*) value;
			break;
		}
	}
}
